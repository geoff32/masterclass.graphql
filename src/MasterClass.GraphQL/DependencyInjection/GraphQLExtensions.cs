using GraphQL;
using GraphQL.Http;
using GraphQL.Server;
using GraphQL.Types;
using GraphQL.Types.Relay;
using MasterClass.GraphQL.Core;
using MasterClass.GraphQL.Mutations;
using MasterClass.GraphQL.Queries;
using MasterClass.GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.GraphQL.DependencyInjection
{
    public static class GraphQLExtensions
    {
        public static IServiceCollection AddMasterClassGraphQL(this IServiceCollection services)
        {
            services.AddSingleton<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService));
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDocumentWriter, DocumentWriter>();

            services.AddTransient(typeof(ConnectionType<>));
            services.AddTransient(typeof(EdgeType<>));
            services.AddTransient<PageInfoType>();
            
            services.AddSingleton<ISchema, MasterClassSchema>();
            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
                options.ExposeExceptions = true;
            })
            .AddDataLoader()
            .AddGraphTypes();

            return services;
        }
    }
}