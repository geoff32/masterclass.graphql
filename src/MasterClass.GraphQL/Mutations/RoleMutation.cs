using System;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;

namespace MasterClass.GraphQL.Mutations
{
    public class RoleMutation : ObjectGraphType
    {
        public RoleMutation(IRoleBusiness roleBusiness)
        {
            Name = "RoleMutation";

            FieldDelegate<IntGraphType>(
                "addRole",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "title", Description = "title of the role"}
                ),
                resolve: (Func<ResolveFieldContext, string, object>)((context, title)
                    => roleBusiness.AddRole(title))
            );

            FieldDelegate<BooleanGraphType>(
                "addUserInRole",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "roleId", Description = "id of the role"},
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "userId", Description = "id of the user"}
                ),
                resolve: (Func<ResolveFieldContext, int, int, object>)((context, roleId, userId)
                    => roleBusiness.AddUserInRole(roleBusiness.GetRole(roleId), userId))
            );
        }
    }
}