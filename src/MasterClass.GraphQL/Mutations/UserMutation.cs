using System;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;

namespace MasterClass.GraphQL.Mutations
{
    public class UserMutation : ObjectGraphType
    {
        public UserMutation(IUserBusiness userBusiness)
        {
            Name = "UserMutation";

            FieldDelegate<IntGraphType>(
                "addUser",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "firstName", Description = "firstName of the user"},
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "lastName", Description = "lastName of the user"}
                ),
                resolve: (Func<ResolveFieldContext, string, string, object>)((context, firstName, lastName)
                    => userBusiness.AddUser(firstName, lastName))
            );
        }
    }
}