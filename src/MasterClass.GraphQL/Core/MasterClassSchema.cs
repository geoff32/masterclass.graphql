using GraphQL;
using GraphQL.Types;
using MasterClass.GraphQL.Types;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassSchema : Schema
    {
        public MasterClassSchema(IDependencyResolver resolver)
            : base(resolver)
        {
            Query = resolver.Resolve<MasterClassQuery>();
            Mutation = resolver.Resolve<MasterClassMutation>();

            RegisterType<EmployeeType>();
            RegisterType<CandidateType>();
        }
    }
}