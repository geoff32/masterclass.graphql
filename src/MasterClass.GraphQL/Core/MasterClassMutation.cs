using System;
using GraphQL.Types;
using MasterClass.GraphQL.Mutations;
using MasterClass.GraphQL.Types;
using MasterClass.Repository.Abstractions;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassMutation : ObjectGraphType<object>
    {
        public MasterClassMutation()
        {
            Name = "Mutation";
            
            Field<UserMutation>("UserMutation", resolve: context => new {} );
            Field<RoleMutation>("RoleMutation", resolve: context => new {} );
        }
    }
}