using GraphQL.Types;
using MasterClass.GraphQL.Queries;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassQuery : ObjectGraphType
    {
        public MasterClassQuery()
        {
            Name = "Query";

            Field<UserQuery>("UserQuery", resolve: context => new {} );
            Field<RoleQuery>("RoleQuery", resolve: context => new {} );
            Field<HumanResourcesQuery>("HumanResourcesQuery", resolve: context => new {} );
        }
    }
}