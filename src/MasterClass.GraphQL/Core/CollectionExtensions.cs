﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphQL.Builders;
using GraphQL.Types.Relay.DataObjects;

namespace MasterClass.GraphQL.Core
{
    public static class CollectionExtensions
    {
        public static Connection<TSource> ToConnection<TSource, TParent, TCursor>(this IEnumerable<TSource> source, ResolveConnectionContext<TParent> context, Func<TSource, TCursor> getCursor)
        {
            var totalCount = source.Count();

            var edges = source
                .Select(item => new Edge<TSource>
                    {
                        Node = item,
                        Cursor = Convert.ToBase64String(Encoding.UTF8.GetBytes(getCursor(item).ToString()))
                    })
                .ToList();
            
            var startCursor = edges.FirstOrDefault()?.Cursor;
            var endCursor = edges.LastOrDefault()?.Cursor;

            var filteredEdges = edges.AsEnumerable();
            if (!string.IsNullOrEmpty(context.Before))
            {
                filteredEdges = filteredEdges.TakeWhile(edge => !edge.Cursor.Equals(context.Before));
            }
            if (!string.IsNullOrEmpty(context.After))
            {
                filteredEdges = filteredEdges.SkipWhile(edge => !edge.Cursor.Equals(context.After)).Skip(1);
            }
            
            edges = filteredEdges
                .Take(context.First.HasValue ? context.First.Value : totalCount)
                .TakeLast(context.Last.HasValue ? context.Last.Value : totalCount)
                .ToList();
            
            var startPageCursor = edges.FirstOrDefault()?.Cursor;
            var endPageCursor = edges.LastOrDefault()?.Cursor;

            var connection = new Connection<TSource>
            {
                Edges = edges,
                PageInfo = new PageInfo {
                    StartCursor = startCursor,
                    EndCursor = endCursor,
                    HasPreviousPage = !string.IsNullOrEmpty(startPageCursor) && !startCursor.Equals(startPageCursor),
                    HasNextPage = !string.IsNullOrEmpty(endPageCursor) && !endCursor.Equals(endPageCursor)
                },
                TotalCount = totalCount
            };

            return connection;
        }
    }
}