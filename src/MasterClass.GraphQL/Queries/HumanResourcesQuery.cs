﻿using GraphQL.Types;
using MasterClass.Business.Abstractions.HumanResources;
using MasterClass.GraphQL.Core;
using MasterClass.GraphQL.Types;

namespace MasterClass.GraphQL.Queries
{
    public class HumanResourcesQuery : ObjectGraphType
    {
        public HumanResourcesQuery(IHumanResourcesBusiness humanResourcesBusiness)
        {
            Name = "HumanResourcesQuery";

            Connection<PersonType>()
                .Name("pagedPersons")
                .Bidirectional()
                .Resolve(context => humanResourcesBusiness.GetAll().ToConnection(context, person => person.Id));

            Field<ListGraphType<PersonType>>()
                .Name("persons")
                .Resolve(context => humanResourcesBusiness.GetAll());

            Field<ListGraphType<EmployeeOrCandidateType>>()
                .Name("employeeOrCandidate")
                .Resolve(context => humanResourcesBusiness.GetAll());
        }
    }
}