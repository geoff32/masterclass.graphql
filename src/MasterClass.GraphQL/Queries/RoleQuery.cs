using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;
using MasterClass.GraphQL.Core;
using MasterClass.GraphQL.Types;
using MasterClass.Repository.Abstractions;

namespace MasterClass.GraphQL.Queries
{
    public class RoleQuery : ObjectGraphType
    {
        public RoleQuery(IRoleBusiness roleBusiness)
        {
            Name = "RoleQuery";

            Connection<RoleType>()
                .Name("pagedRoles")
                .Bidirectional()
                .Resolve(context => roleBusiness.GetAllRoles().ToConnection(context, role => role.Id));

            Field<ListGraphType<RoleType>>()
                .Name("roles")
                .Resolve(context => roleBusiness.GetAllRoles());
        }
    }
}