using System;
using GraphQL.Types;
using GraphQL.Types.Relay.DataObjects;
using MasterClass.Business.Abstractions.Users;
using MasterClass.GraphQL.Core;
using MasterClass.GraphQL.Types;

namespace MasterClass.GraphQL.Queries
{
    public class UserQuery : ObjectGraphType
    {
        public UserQuery(IUserBusiness userBusiness)
        {
            Name = "UserQuery";

            Connection<UserType>()
                .Name("pagedUsers")
                .Bidirectional()
                .Resolve(context => userBusiness.GetAllUsers().ToConnection(context, user => user.Id));

            Field<ListGraphType<UserType>>()
                .Name("users")
                .Resolve(context => userBusiness.GetAllUsers());

            FieldDelegate<UserType>(
                "user",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id", Description = "id of the user"}),
                resolve: (Func<ResolveFieldContext, int, object>)((context, id) => userBusiness.GetUser(id))
            );
        }
    }
}