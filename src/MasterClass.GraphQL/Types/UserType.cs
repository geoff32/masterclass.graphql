using GraphQL.Types;
using MasterClass.Business.Users.Models;

namespace MasterClass.GraphQL.Types
{
    public class UserType : ObjectGraphType<User>
    {
        public UserType()
        {
            Name = "User";
            
            Field(u => u.Id).Description("The id of the user.");
            Field(u => u.LastName).Description("The lastname of the user.");
            Field(u => u.FirstName).Description("The firstname of the user.");
        }
    }
}