using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class PersonType : InterfaceGraphType<Person>
    {
        public PersonType()
        {
            Name = "Person";

            Field(p => p.Id).Description("The identifier of the person");
            Field(p => p.Name).Description("The name of the person");
        }
    }
}