using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;
using MasterClass.Business.Users.Models;

namespace MasterClass.GraphQL.Types
{
    public class RoleType : ObjectGraphType<Role>
    {
        private readonly IUserBusiness _userBusiness;

        public RoleType(IDataLoaderContextAccessor accessor, IUserBusiness userBusiness)
        {
            _userBusiness = userBusiness;

            Name = "Role";
            
            Field(r => r.Id).Description("The id of the profile.");
            Field(r => r.Title).Description("The lastname of the user.");
            Field<ListGraphType<UserType>>()
                .Name(nameof(Role.Users))
                .Description("Users belonging to the profile.")
                .ResolveAsync(async context => {
                    if (context.SubFields.Keys.All(name => name.Equals(nameof(Role.Id), StringComparison.OrdinalIgnoreCase)))
                    {
                        return context.Source.Users.Select(id => new User { Id = id });
                    }
                    
                    var loader = accessor.Context.GetOrAddBatchLoader<IEnumerable<long>, IEnumerable<User>>("GetUsersById", LoadUserByIdAsync);

                    return await loader.LoadAsync(context.Source.Users);
                });
        }

        private async Task<IDictionary<IEnumerable<long>, IEnumerable<User>>> LoadUserByIdAsync(IEnumerable<IEnumerable<long>> ids, CancellationToken token)
        {
            var users = await Task.FromResult(_userBusiness.GetUsers(ids.SelectMany(id => id).Distinct()).ToDictionary(u => u.Id));

            return ids.ToDictionary(g => g, g => g.Select(id => users[id]));
        }
    }
}