using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class CandidateType : ObjectGraphType<Candidate>
    {
        public CandidateType()
        {
            Name = "Candidate";

            Interface<PersonType>();

            Field(p => p.Id).Description("The identifier of the candidate");
            Field(p => p.Name).Description("The name of the candidate");
            
            Field<EmployeeType>(nameof(Candidate.Recruiter), "The recruiter who contacted the candidate.");
            Field(c => c.ContactDate).Description("The date of first contact.");
        }
    }
}