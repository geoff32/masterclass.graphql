using GraphQL.Types;

namespace MasterClass.GraphQL.Types
{
    public class EmployeeOrCandidateType : UnionGraphType
    {
        public EmployeeOrCandidateType()
        {
            Type<EmployeeType>();
            Type<CandidateType>();
        }
    }
}