using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class EmployeeType : ObjectGraphType<Employee>
    {
        public EmployeeType()
        {
            Name = "Employee";
            
            Interface<PersonType>();

            Field(p => p.Id).Description("The identifier of the employee");
            Field(p => p.Name).Description("The name of the employee");
            
            Field(e => e.EntryDate).Description("The entry date of the employee.");
        }
    }
}