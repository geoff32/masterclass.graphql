# Interfaces

## Création de l'interface

Pour créer une interface il faut hériter de la classe `InterfaceGraphType`  

> echo . > ./src/MasterClass.GraphQL/Types/PersonType.cs  

```c#
using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class PersonType : InterfaceGraphType<Person>
    {
        public PersonType()
        {
            Name = "Person";

            Field(p => p.Id).Description("The identifier of the person");
            Field(p => p.Name).Description("The name of the person");
        }
    }
}
```

## Implémentation  

Nous allons implémenter l'interface pour 2 objets  

> echo . > ./src/MasterClass.GraphQL/Types/EmployeeType.cs  

```c#
using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class EmployeeType : ObjectGraphType<Employee>
    {
        public EmployeeType()
        {
            Name = "Employee";
            
            Interface<PersonType>();

            Field(p => p.Id).Description("The identifier of the employee");
            Field(p => p.Name).Description("The name of the employee");
            
            Field(e => e.EntryDate).Description("The entry date of the employee.");
        }
    }
}
```

> echo . > ./src/MasterClass.GraphQL/Types/CandidateType.cs  

```c#
using GraphQL.Types;
using MasterClass.Business.HumanResources.Models;

namespace MasterClass.GraphQL.Types
{
    public class CandidateType : ObjectGraphType<Candidate>
    {
        public CandidateType()
        {
            Name = "Candidate";

            Interface<PersonType>();

            Field(p => p.Id).Description("The identifier of the candidate");
            Field(p => p.Name).Description("The name of the candidate");
            
            Field<EmployeeType>(nameof(Candidate.Recruiter), "The recruiter who contacted the candidate.");
            Field(c => c.ContactDate).Description("The date of first contact.");
        }
    }
}
```

Il faut bien penser à rajouter nos types dans le schéma  

```c#
RegisterType<EmployeeType>();
RegisterType<CandidateType>();
```