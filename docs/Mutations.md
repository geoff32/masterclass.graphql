# Mutations  

## Mise en place  

Création d'une mutation pour nos Users  

> mkdir ./src/MasterClass.GraphQL/Mutations  
echo . > ./src/MasterClass.GraphQL/Mutations/UserMutation.cs  

```c#
using System;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;

namespace MasterClass.GraphQL.Mutations
{
    public class UserMutation : ObjectGraphType
    {
        public UserMutation(IUserBusiness userBusiness)
        {
            Name = "UserMutation";

            FieldDelegate<IntGraphType>(
                "addUser",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "firstName", Description = "firstName of the user"},
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "lastName", Description = "lastName of the user"}
                ),
                resolve: (Func<ResolveFieldContext, string, string, object>)((context, firstName, lastName)
                    => userBusiness.AddUser(firstName, lastName))
            );
        }
    }
}
```

Ajout d'une mutation racine  

> echo . > ./src/MasterClass.GraphQL/Core/MasterClassMutation.cs  

```c#
using GraphQL.Types;
using MasterClass.GraphQL.Mutations;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassMutation : ObjectGraphType<object>
    {
        public MasterClassMutation()
        {
            Name = "Mutation";
            
            Field<UserMutation>("UserMutation", resolve: context => new {} );
        }
    }
}
```

Ajout de la mutation dans le constructeur de notre schéma `MasterClassSchema`  

```c#
Mutation = resolver.Resolve<MasterClassMutation>();
```

## Exemple de requête

```graphql
mutation {
  userMutation {
    addUser(firstName: "add firstname1", lastName: "add lastname1")
  }
}
```