# Outils

## GraphiQL

Nous avons utilisé `Playground` mais il existe également `GraphiQL` qui est un IDE qui nous permet de faire nos requêtes
Pour l'utiliser il faut avant tout référencer le package nuget `GraphQL.Server.Ui.GraphiQL`  

> dotnet add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj package GraphQL.Server.Ui.GraphiQL -v 3.4.0  

Et on ajoute le middleware dans notre classe `StartUp`

```c#
app.UseGraphQLVoyager(new GraphQLVoyagerOptions());
```

## Voyager  

Voyager est une UI qui permet de visualiser les dépendences entre les objets de notre schéma.  
Pour l'utiliser il faut avant tout référencer le package nuget `GraphQL.Server.Ui.Voyager`  

> dotnet add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj package GraphQL.Server.Ui.Voyager -v 3.4.0  

Et on ajoute le middleware dans notre classe `StartUp`

```c#
app.UseGraphQLVoyager(new GraphQLVoyagerOptions());
```