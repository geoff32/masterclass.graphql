# Initialisation  

## Création des projets  

Création de la solution :  

> dotnet new sln -n MasterClass.GraphQL  

Création du projet web et du projet repository :  

> mkdir src  
cd src  
dotnet new web -n MasterClass.GraphQL  
cd ..  

Ajout des projets dans la solution  

> dotnet sln add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj  

Référencement d'un package nuget pour avoir à disposition un jeu de données

> dotnet add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj package MasterClass.Data  

Ce package vous offre la possibilité de travailler en mémoire ou via une base redis dans un docker. Si vous travaillez en mémoire, les données seront réinitialisées à chaque redémarrage de l'application  
La méthode d'extension `UseMasterClassDataManager` vous permet d'ajouter une route à votre application `/data/management/initialize` qui permet d'initialiser (ou de réinitialiser) un jeu de données  

```c#
using MasterClass.Data.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.GraphQL
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMasterClassData();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMasterClassDataManager();
        }
    }
}

```

Si vous souhaitez faire persister les données vous pouvez utiliser redis (dans un conteneur docker) :  

```docker
docker run --rm -d --name redis-graphql -p 6379:6379 -v /data/redis:/data redis redis-server --appendonly yes  
```

Puis ajoutez les 2 variable d'environnements suivantes pour que le package `MasterClass.Data` utlise redis  

- "MASTERCLASS_REDIS_HOST": "localhost:6379"  
- "MASTERCLASS_REDIS_ENABLED": "true"  

## Mise en place de GraphQL  

Ajoutons le package nuget pour GraphQL  

> dotnet add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj package GraphQL.Server.Transports.AspNetCore -v 3.4.0  

Nous allons également ajouter un package qui nous permettra d'utiliser *Playground*, une UI qui nous permettra de tester nos requêtes  

> dotnet add ./src/MasterClass.GraphQL/MasterClass.GraphQL.csproj package GraphQL.Server.Ui.Playground -v 3.4.0 

Et ajouter le middleware dans la classe Startup  

```c#
app.UseGraphQLPlayground(new GraphQLPlaygroundOptions());
```