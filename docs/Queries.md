# Queries  

## Mise en place

Création d'un type de données GraphQL basé sur notre type `User`  

> mkdir ./src/MasterClass.GraphQL/Types  
echo . > ./src/MasterClass.GraphQL/Types/UserType.cs  

```c#
using GraphQL.Types;
using MasterClass.Business.Users.Models;

namespace MasterClass.GraphQL.Types
{
    public class UserType : ObjectGraphType<User>
    {
        public UserType()
        {
            Name = "User";
            
            Field(u => u.Id).Description("The id of the user.");
            Field(u => u.LastName).Description("The lastname of the user.");
            Field(u => u.FirstName).Description("The firstname of the user.");
        }
    }
}
```

Création de la Query  

> mkdir ./src/MasterClass.GraphQL/Queries  
echo . > ./src/MasterClass.GraphQL/Queries/UserQuery.cs  

```c#
using System;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;
using MasterClass.GraphQL.Types;

namespace MasterClass.GraphQL.Queries
{
    public class UserQuery : ObjectGraphType
    {
        public UserQuery(IUserBusiness userBusiness)
        {
            Name = "UserQuery";

            Field<ListGraphType<UserType>>("users", resolve: context => userBusiness.GetAllUsers());
            FieldDelegate<UserType>(
                "user",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id", Description = "id of the user"}),
                resolve: (Func<ResolveFieldContext, int, object>)((context, id) => userBusiness.GetUser(id))
            );
        }
    }
}
```

Création du schéma

> mkdir ./src/MasterClass.GraphQL/Core  
echo . > ./src/MasterClass.GraphQL/Core/MasterClassSchema.cs  

```c#
using GraphQL;
using GraphQL.Types;
using MasterClass.GraphQL.Queries;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassSchema : Schema
    {
        public MasterClassSchema(IDependencyResolver resolver)
            : base(resolver)
        {
            Query = resolver.Resolve<UserQuery>();
        }
    }
}
```

Création de méthodes d'extensions afin d'enregistrer les services nécessaires à GraphQL

> mkdir ./src/MasterClass.GraphQL/DependencyInjection  
echo . > ./src/MasterClass.GraphQL/DependencyInjection/GraphQLExtensions.cs  

```c#
using GraphQL;
using GraphQL.Http;
using GraphQL.Server;
using GraphQL.Types;
using MasterClass.GraphQL.Core;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.GraphQL.DependencyInjection
{
    public static class GraphQLExtensions
    {
        public static IServiceCollection AddMasterClassGraphQL(this IServiceCollection services)
        {
            services.AddSingleton<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService));
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            
            services.AddSingleton<ISchema, MasterClassSchema>();
            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
                options.ExposeExceptions = true;
            })
            .AddGraphTypes();

            return services;
        }
    }
}
```

Ajout des services GraphQL dans la classe Startup

```c#
services.AddMasterClassGraphQL();
```

Ajout du middleware GraphQL dans la classe Startup

```c#
// add http for Schema at default url /graphql
app.UseGraphQL<ISchema>("/graphql");
```

## Exemples de requêtes

Nous allons pouvoir effectuer nos premières requêtes graphql en utilisant l'interface https://localhost:5001/ui/playground  

```graphql
query {
  users {
    lastName
  }
}
```
Comme nous le voyons, la `query` ne demande que le `field` lastName de notre user. Nous pouvons lui demander également de préciser le champ firstName  

```graphql
query {
  users {
    firstName
    lastName
  }
}
```

Nous avons également créé une `query` qui prend un `argument` afin de récupérer un user à partir de son id  

```graphql
query {
  users {
    firstName
    lastName
  },
  user(id: 1) {
    lastName
  }
}
```

Nous pouvons utiliser des `fragments` afin d'homogénéiser le format de nos user  

```graphql
query {
  users {
    ...UserDisplay
  },
  user(id: 1) {
    ...UserDisplay
  }
}

fragment UserDisplay on User {
  firstName
  lastName
}
```

Nous pouvons également utiliser des `inline fragments`

```graphql
query {
  user(id: 1) {
    ... on User {
      firstName
      lastName
    }
  }
}

fragment UserDisplay on User {
  firstName
  lastName
}
```

Il est également possible d'utiliser des `alias` ce qui a pour effet de modifier le nom du noeud dans notre réponse  

```graphql
{
  users {
    ...UserDisplay
  }
  user1: user(id: 1) {
    ...UserDisplay
  }
}

fragment UserDisplay on User {
  firstName
  lastName
}
```

Nous pouvons utiliser des `variables`

```graphql
query ($userId:Int!){
  users {
    ...UserDisplay
  }
  user1: user(id: $userId) {
    ...UserDisplay
  }
}

fragment UserDisplay on User {
  firstName
  lastName
}
```  

```json
{
  "userId": 2
}
```

A l'aides `variables` nous pouvons utiliser des `directives` (`skip` ou `include`)  

```graphql
query($userId: Int!, $hideUserId: Boolean!) {
  user1: user(id: $userId) {
    id @skip(if: $hideUserId)
    firstName
    lastName
  }
}
```  

```json
{
  "userId": 2,
  "hideUserId": true
}
```

```graphql
query ($userId:Int! $showUserId: Boolean!){
  users {
    ...UserDisplay
  }
  user1: user(id: $userId) {
    ...UserDisplay
  }
}

fragment UserDisplay on User {
  id @include(if: $showUserId)
  firstName
  lastName
}
```  

```json
{
  "userId": 2,
  "showUserId": true
}
```

## Organisation des queries

Création d'un object GraphQL pour exposer nos roles  

> echo . > ./src/MasterClass.GraphQL/Types/RoleType.cs  

```c#
using System.Linq;
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;
using MasterClass.Business.Users.Models;

namespace MasterClass.GraphQL.Types
{
    public class RoleType : ObjectGraphType<Role>
    {
        public RoleType(IUserBusiness userBusiness)
        {
            Name = "Role";
            
            Field(r => r.Id).Description("The id of the profile.");
            Field(r => r.Title).Description("The lastname of the user.");
            Field<ListGraphType<UserType>>()
                .Name(nameof(Role.Users))
                .Description("Users belonging to the profile.")
                .Resolve(context => context.Source.Users.Select(id => userBusiness.GetUser(id)));
        }
    }
}
```

Création de la query  

> echo . > ./src/MasterClass.GraphQL/Queries/RoleQuery.cs  

```c#
using GraphQL.Types;
using MasterClass.Business.Abstractions.Users;
using MasterClass.GraphQL.Types;

namespace MasterClass.GraphQL.Queries
{
    public class RoleQuery : ObjectGraphType
    {
        public RoleQuery(IRoleBusiness roleBusiness)
        {
            Name = "RoleQuery";

            Field<ListGraphType<RoleType>>("roles", resolve: context => roleBusiness.GetAllRoles());
        }
    }
}
```

Création d'une query racine  

> echo . > ./src/MasterClass.GraphQL/Core/MasterClassQuery.cs  

```c#
using GraphQL.Types;
using MasterClass.GraphQL.Queries;

namespace MasterClass.GraphQL.Core
{
    public class MasterClassQuery : ObjectGraphType
    {
        public MasterClassQuery()
        {
            Name = "Query";

            Field<UserQuery>("UserQuery", resolve: context => new {} );
            Field<RoleQuery>("RoleQuery", resolve: context => new {} );
        }
    }
}
```

Modification de la query dans notre schéma  

```c#
Query = resolver.Resolve<MasterClassQuery>();
```

Requête GraphQL  

```graphql
query($userId: Int!) {
  userQuery {
    users {
      ...UserDisplay
    }
    user1: user(id: $userId) {
      ...UserDisplay
    }
  }
  roleQuery {
    roles {
      ...RoleDisplay
    }
  }
}

fragment RoleDisplay on Role {
  title
  users {
    ...UserDisplay
  }
}

fragment UserDisplay on User {
  firstName
  lastName
}
```