# Unions  

## Mise en place  

Création d'un type qui hérite de `UnionGraphType`  

> echo . > ./src/MasterClass.GraphQL/Types/EmployeeOrCandidateType.cs  

```c#
using GraphQL.Types;

namespace MasterClass.GraphQL.Types
{
    public class EmployeeOrCandidateType : UnionGraphType
    {
        public EmployeeOrCandidateType()
        {
            Type<EmployeeType>();
            Type<CandidateType>();
        }
    }
}
```

Et ajout d'un field afin de pouvoir requêter  

```c#
Field<ListGraphType<EmployeeOrCandidateType>>()
    .Name("employeeOrCandidate")
    .Resolve(context => humanResourcesBusiness.GetAll());
```

## Exemple de requêtes  

```graphql
query {
  humanResourcesQuery {
    employeeOrCandidate {
      __typename
      ... on Employee {
        id
        name
        entryDate
      }
      ... on Candidate {
        id
        name
        contactDate
        recruiter {
          name
        }
      }
    }
  }
}
```