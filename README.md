# GraphQL et .NET Core  

Query :  

```graphql
query masterclass {
  userQuery {
    allUsers: users {
      ...UserDisplay
    }
  }
  roleQuery {
    allRoles: roles {
      ...RoleDisplay
    }
  }
}

query user($userId: Int!) {
  userQuery {
    user(id: $userId) {
      ...UserDisplay
    }
  }
}

mutation addRole($newRoleTitle: String!) {
  roleMutation {
    addRole(title: $newRoleTitle)
  }
}

mutation addUserInRole($roleId: Int!, $userId: Int!) {
  roleMutation {
    addUserInRole(roleId: $roleId, userId: $userId)
  }
}

mutation addUser($newUserFirstName: String!, $newUserLastName: String!) {
  userMutation {
    addUser(firstName: $newUserFirstName, lastName: $newUserLastName)
  }
}

fragment UserDisplay on User {
  id
  firstName
  lastName
}

fragment RoleDisplay on Role {
  id
  title
  users {
    ...UserDisplay
  }
}
```

Variables :  

```json
{
  "newUserFirstName": "John",
  "newUserLastName": "Doe",
  "userId": 2,
  "newRoleTitle": "Users",
  "roleId": 1
}
```